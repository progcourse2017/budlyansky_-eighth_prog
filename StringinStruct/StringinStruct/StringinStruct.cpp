// StringinStruct.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

struct IandS
{
	int integer;
	string line;
};

void enterTheData(const int count,IandS * userStruct);
void showData(IandS * yourStruct, const int count);
IandS * addStruct(const int count, IandS * yourStruct);


int main()
{
	IandS * userStruct = 0;
	int countStruct = 0;
	int key = false;
	string choise;

	while(!key)
	{
		userStruct = addStruct(countStruct, userStruct);
		enterTheData(countStruct, userStruct);

		countStruct++;

		cout << "Create new object for structure?(yes/no): ";
		cin >> choise;

		if (choise == "yes")
		{
			continue;
		}
		else if (choise =="no")
		{
			key = true;
		}
		else
		{
			cout << "Incorrect choise, breaking. . .";
			break;
		}

	}

	cout << "Show you data?(yes/no):";
	cin >> choise;

	if(choise == "yes")
		showData(userStruct,countStruct);

	cout << endl;

	system("pause");

    return 0;
}

void enterTheData(const int count,IandS * yourStruct)
{

	cout << "Any integer value: ";
	cin >> yourStruct[count].integer;

	cout << "Any string: ";
	cin >> yourStruct[count].line;

};

void showData(IandS * yourStruct, const int count)
{
	for(int i=0;i<count;i++)
	{
		cout <<"\n"<< i+1 << ". "
			<< yourStruct[i].integer << ' '
			<< yourStruct[i].line;
	}
};

IandS * addStruct(const int count, IandS * yourStruct)
{
	if(count == 0)
	{
		yourStruct = new IandS[count+1];
	}
	else
	{
		IandS* copy = new IandS[count + 1];

		for (int i = 0; i < count; i++)
		{
			copy[i] = yourStruct[i];
		}

		delete[]yourStruct;

		yourStruct = copy;
	}

	return yourStruct;

};